<h1><a id="Auto_Installation_of_GitLab_via_Ansible_0"></a>Auto Installation of GitLab via Ansible</h1>
<p>This assumes that Ansible is availabel and ssh to the linux VM has been established.</p>
<p>Open the gitlab-install-playbook.yml and set the variables as applicable<br>
<em>#Disclaimer#</em>: The variable currently are in the native YAML (poor practice) so recommend that you pull the repo onto the VM where Ansible is running and run it locally for now as to keep unames and pwd out of the repo</p>
<p>The gitlab_rpm: is contained inside the .tar file that gets pulled from Nexus you just need to know which RPM is to be installed</p>
<ul>
<li>ie gitlab-ce-11.9.10-ce.0.el7.x86_64.rpm  — for version 11.9.10-ce for RHEL7</li>
</ul>
<p>This playbook will install rpms that are known to be in the CIE, but does create a local repo and will install other rpms from that repo to include the gitlab rpm</p>
<p>When the GitLab RPM runs it produces the URL to where you are able to access GitLab once the installation completes.  This is captured in /tmp/gitlab_install.txt</p>
<ul>
<li>cat this file and you will see the URL toward the bottom <em>NOTE</em> ignore the reference to reconfigure GitLab as the playbook has already done this</li>
</ul>
<p><em>#Disclaimer#</em> There is much more that can be done with this playbook for the the early use case this should suffice with the expecation that it will be improved over time.</p>
<h1><a id="To_replicate_this_you_will_need_Ansible_and_two_VMs_18"></a>To replicate this you will need Ansible and two VMs</h1>
<h2><a id="Installing_Ansible_19"></a>Installing Ansible</h2>
<p>Install curl</p>
<pre><code class="language-sh">sudo yum install curl
</code></pre>
<p>Install wget</p>
<pre><code class="language-sh">sudo yum install wget
</code></pre>
<p>Install Ansible</p>
<pre><code class="language-sh"><span class="hljs-built_in">cd</span> /tmp
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-<span class="hljs-number">7</span>.noarch.rpm
sudo yum install epel-release-latest-<span class="hljs-number">7</span>.noarch.rpm
sudo yum install ansible
</code></pre>
<p>Install pip</p>
<pre><code class="language-sh">wget https://pypi.python.org/packages/<span class="hljs-built_in">source</span>/s/setuptools/setuptools-<span class="hljs-number">7.0</span>.tar.gz --no-check-certificate
tar xzf setuptools-<span class="hljs-number">7.0</span>.tar.gz
<span class="hljs-built_in">cd</span> setuptools-<span class="hljs-number">7.0</span>
sudo python setup.py install
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
pip --version -- should present version number
</code></pre>
<p>Set remote hosts in /etc/hosts</p>
<pre><code class="language-sh">sudo vi /etc/hosts
</code></pre>
<ul>
<li>add the hosts IP and hostname
<ul>
<li>EX: 192.162.1.1     remote-host<br>
create an Ansible user (these steps should be run on all hosts to include the master)</li>
</ul>
</li>
</ul>
<pre><code class="language-sh">sudo useradd <span class="hljs-operator">-d</span> /home/ -m 
EX -- sudo useradd <span class="hljs-operator">-d</span> /home/ansadm -m ansadm
sudo passwd 
EX -- sudo passwd ansadm
</code></pre>
<p>Master on AWS is Temp1234</p>
<ul>
<li>sudo passwd -x -1
<ul>
<li>EX sudo passwd -x -1 ansadm</li>
</ul>
</li>
<li>above is a numberic one not lower case l<br>
Assume the new user</li>
<li>su
<ul>
<li>EX – su ansadm<br>
generate a key pair for establishing ssh on the master</li>
</ul>
</li>
</ul>
<pre><code class="language-sh">ssh-keygen -t rsa
</code></pre>
<ul>
<li>accept default path</li>
<li>do not set password<br>
set permissions on the .ssh dir and key pair</li>
</ul>
<pre><code class="language-sh">chmod <span class="hljs-number">700</span> .ssh
chown &lt;user&gt;:&lt;group&gt; .ssh
EX -- chown ansadm:ansadm .ssh
</code></pre>
<p>create authorized_keys file</p>
<pre><code class="language-sh">cp id_rsa.pub authorized_keys
chown : authorized_keys
EX -- chown ansadm:ansadm authorized_keys
chmod <span class="hljs-number">600</span> authorized_keys
</code></pre>
<p>copy the id_rsa.pub key to the remote severs from master</p>
<ul>
<li>to ssh in AWS you will need to use the .pem first to push the .pub key over to the remote hosts</li>
<li>ssh-copy-id -i @&lt;remote_host&gt;</li>
<li>AWS_EX – ssh-copy-id -i .pem <a href="mailto:ansadm@ec2-3-82-117-36.compute-1.amazonaws.com">ansadm@ec2-3-82-117-36.compute-1.amazonaws.com</a>
<ul>
<li>may have to just cat the authorized key file on the master and copy into the authorized_keys file on the remote host<br>
Add the user on all hosts to the sudoer file</li>
</ul>
</li>
</ul>
<pre><code class="language-sh">sudo vi /etc/sudoers
</code></pre>
<ul>
<li>add the user as a sudoer</li>
<li>shft + G – takes you to the end of the file<br>
copy the existing user change the name</li>
<li>EX – ansadm   ALL=(ALL)     NOPASSWD: ALL</li>
</ul>
<h1><a id="Test_97"></a>Test</h1>
<h2><a id="add_hosts_to_this_file_98"></a>add hosts to this file</h2>
<pre><code class="language-sh"><span class="hljs-built_in">cd</span> /etc/ansible
sudo vi hosts
ansible -m shell <span class="hljs-operator">-a</span> whoami all
</code></pre>
<p>------------ should see something like ----------------------<br>
<a href="http://ec2-3-83-119-38.compute-1.amazonaws.com">ec2-3-83-119-38.compute-1.amazonaws.com</a> | CHANGED | rc=0 &gt;&gt; ansadm</p>